import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'app-visit-survey',
  styleUrl: 'app-visit-survey.css'
})
export class AppVisitSurvey {

  @Prop() surveyId: number;

  onListItemClicked(event) {
    if(event.target.tagName.toLowerCase() !== 'ion-item') return;

    const item = event.target as HTMLIonItemElement;
    const list = item.parentElement as HTMLIonListElement;

    list.querySelectorAll('ion-item').forEach((element: HTMLIonItemElement) => {
      element.color = '';
    });

    item.color = item.color !== 'primary' ? 'primary' : '';
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>
            Visit Survey
          </ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <ion-slides style={{
          '--bullet-background-active': '#555555',
          '--bullet-background': 'var(--ion-color-step-200, #555555)',
          height: '100%'
        }} pager={true}>
          <ion-slide>
            <ion-card style={{'--background': 'white'}}>
              <ion-card-header>
                <ion-card-title>
                  Were you satisfied with your visit?
                </ion-card-title>
              </ion-card-header>
              <ion-card-content>
                <ion-list onClick={(evt) => this.onListItemClicked(evt)}>
                  <ion-item id="1" button lines="none">
                    <ion-icon name="thumbs-down" slot="start"/>
                    Not Satisfied
                  </ion-item>
                  <ion-item id="2" button lines="none">
                    <ion-icon name="thumbs-up" slot="start"/>
                    Satisfied
                  </ion-item>
                </ion-list>
              </ion-card-content>
            </ion-card>
          </ion-slide>
          <ion-slide>

          </ion-slide>
        </ion-slides>
      </ion-content>
    ];
  }
}
