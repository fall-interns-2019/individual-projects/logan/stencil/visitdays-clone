import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-itinerary-page',
  styleUrl: 'app-itinerary-page.css'
})
export class AppItineraryPage {

  render() {
    return [
      <app-itinerary-header/>,
      <ion-content color="light" fullscreen>
        <ion-list style={{'background': 'var(--ion-color-light)'}}>
          <ion-list-header>
            <ion-label color="primary" class="bold-text">
              Today
            </ion-label>
          </ion-list-header>
          <ion-card style={{'--background': 'white'}}>
            <ion-card-content>
              <ion-item lines="none">
                <ion-icon name="calendar" slot="start"/>
                Morning Campus Tour
              </ion-item>
              <ion-item lines="none">
                <ion-icon name="time" slot="start"/>
                9:00am - 10:00am
              </ion-item>
              <ion-item lines="none">
                <ion-icon name="pin" slot="start"/>
                Admissions Office
              </ion-item>
            </ion-card-content>
          </ion-card>
        </ion-list>
      </ion-content>
    ];
  }
}
