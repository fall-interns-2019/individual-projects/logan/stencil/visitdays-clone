import {Component, Element, h} from '@stencil/core';
import L, {LatLngExpression} from 'leaflet';
import {delay} from "../../helpers/utils";

@Component({
  tag: 'app-test',
  styleUrl: '../../../node_modules/leaflet/dist/leaflet.css'
})
export class AppTest {

  @Element() el: HTMLElement;

  private map;

  async componentDidRender() {
    const center: LatLngExpression = [38.9049825, -77.0045437];
    const layer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>\'',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoibG9nYXZvbyIsImEiOiJjazBvN25rNDAwN3NsM2xuOGIwbTM2MnJhIn0.dB2Kce4PTC2Zq-UssBjvUA'
    });

    this.map = L.map('map', {
      layers: [layer]
    }).setView(center, 14);

    L.marker(center, {
      icon: L.icon({
        iconUrl: '/assets/marker-icon.png',
        shadowUrl: '/assets/marker-shadow.png'
      })
    }).addTo(this.map)
      .bindPopup('Fullmeasure University')
      .openPopup();

    await delay(300);
    this.map.invalidateSize();
    // this.map.setView(center, 16);
  }

  render() {
    return [
      <div id="map" style={{'height': '50vh', 'display': 'block'}}/>
    ];
  }
}
