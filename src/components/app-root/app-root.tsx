import {Component, Element, h, Prop} from '@stencil/core';
import {toastController} from "@ionic/core";
import {ApiHelper} from "../../helpers/apiHelper";

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  @Element() el: HTMLElement;

  @Prop() ApiHelper: ApiHelper;

  private static routes = [
    {url: '/', access: 'default'},
    {url: '/register', access: 'default'},
    {url: '/login', access: 'default'},
    {url: '/visit*', access: 'default'},
    {url: '/survey*', access: 'default'},
    {url: '/schedule', access: 'default'},
    {url: '/test*', access: 'default'}
  ];

  constructor() {
    this.ApiHelper = new ApiHelper();
  }

  static getApiHelper() {
    return document.querySelector('app-root').ApiHelper;
  }

  static async route(url: string, keepHistory?: boolean) {
    document.querySelector('ion-router').push(url, !keepHistory ? 'root' : undefined);
  }

  static async checkAccess(routePath) {
    let routeInfo = AppRoot.routes.find((route) => {
      return route.url.includes('*')
        ? routePath.match(route.url)
        : routePath === route.url;
    });
    if (!routeInfo) return false;

    let validUser = await AppRoot.getApiHelper().validateUser();
    let destination;

    if (typeof routeInfo.access === 'string') {
      if (routeInfo.access === 'default' && validUser) {
        destination = '/';
      } else if (routeInfo.access === 'default_auth') {
        if (validUser) {
          await AppRoot.getApiHelper().waitForUser();
        } else {
          destination = '/login';
        }
      }
    } else {
      // custom redirecting here
    }

    return [!destination, destination] as any;
  }

  private static async onIonRouteWillChange(event) {
    const [access, redirect] = await AppRoot.checkAccess(event.detail.to);

    console.log('onIonRouteWillChange: trying (', event.detail.to, ') access(', access, ') redirect(', redirect, ')');

    if (!access) {
      console.log('onIonRouteWillChange: no access! will redirect!');
      window.location.href = redirect;
    }
  }

  static async showNotification(message, color?, waitForDismiss?) {
    const toast = await toastController.create({
      message, color,
      duration: 2000,
      position: 'middle',
    });

    await toast.present();

    if (waitForDismiss)
      await toast.onWillDismiss();
  }

  render() {
    return (
      <ion-app>
        <ion-router useHash={false} onIonRouteWillChange={(evt) => AppRoot.onIonRouteWillChange(evt)}>
          <ion-route url="/" component="app-home-page"/>
          <ion-route url="/register" component="app-register-page"/>
          <ion-route url="/login" component="app-login-page"/>
          <ion-route url="/schedule" component="app-itinerary-page"/>
          <ion-route url="/visit/:collegeId" component="app-visit-tabs"/>
          <ion-route url="/survey/:surveyId" component="app-visit-survey"/>

          {/* test routes */}
          <ion-route url="/test1" component="app-visit-modal"/>
          <ion-route url="/test2" component="app-test"/>
        </ion-router>
        <ion-nav/>
      </ion-app>
    );
  }
}
