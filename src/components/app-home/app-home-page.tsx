import {Component, h, State} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'app-home-page',
  styleUrl: 'app-home-page.css'
})
export class AppHomePage {

  @State() selectedCollegeId: number;

  onIonSelectChanged(event) {
    this.selectedCollegeId = event.detail.value;
  }

  async navigateToRegistration() {
    await AppRoot.route(`/visit/${this.selectedCollegeId}`);
  }

  render() {
    return [
      <ion-content>
        <div class="background-container">
          <ion-grid style={{'background-color': 'rgba(0, 0, 0, 0.5)'}}>
            <ion-row style={{height: '100vh'}}
                     align-items-center justify-content-around>
              <ion-col>
                <ion-title style={{'font-size': '2em'}} color="light" class="ion-padding center-horiz">
                  Welcome
                </ion-title>
                <ion-item style={{'--background-hover': 'white'}}
                          class="ion-padding center-horiz button-margin">
                  <ion-label>Select your institution</ion-label>
                  <ion-select onIonChange={(evt) => this.onIonSelectChanged(evt)}>
                    <ion-select-option value={1}>Fullmeasure University</ion-select-option>
                  </ion-select>
                </ion-item>
                <ion-button class="ion-padding center-horiz button-margin half-width"
                            color="primary" disabled={!this.selectedCollegeId}
                            onClick={() => this.navigateToRegistration()}>
                  Get Started!
                </ion-button>
              </ion-col>
            </ion-row>
          </ion-grid>
        </div>
      </ion-content>
    ];
  }
}
