import {Component, Element, h, Prop} from '@stencil/core';
import L, {LatLngExpression} from 'leaflet';

@Component({
  tag: 'app-visit-travel-page',
  styleUrl: '../../../node_modules/leaflet/dist/leaflet.css'
})
export class AppVisitTravelPage {

  @Element() el: HTMLElement;

  @Prop() collegeId: number;

  private map;

  async componentDidRender() {
    const center: LatLngExpression = [38.9049825, -77.0045437];
    const layer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      // attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>\'',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoibG9nYXZvbyIsImEiOiJjazBvN25rNDAwN3NsM2xuOGIwbTM2MnJhIn0.dB2Kce4PTC2Zq-UssBjvUA'
    });

    this.map = L.map('map', {
      layers: [layer],
      attributionControl: false
    }).setView(center, 16);

    L.marker(center, {
      icon: L.icon({
        iconUrl: '/assets/marker-icon.png',
        shadowUrl: '/assets/marker-shadow.png'
      })
    }).addTo(this.map)
      .bindPopup('<p class="bold-text ion-form-text">Fullmeasure University</p>', {
        maxWidth: 600,
        maxHeight: 100,
        closeButton: false,
        autoClose: false,
        closeOnEscapeKey: false,
        closeOnClick: false
      })
      .openPopup();

    L.Control['Directions'] = L.Control.extend({
      onAdd: () => {
        const button = L.DomUtil.create('ion-button');
        button.textContent = 'Directions';
        button.addEventListener('click', () => {
          console.log('directions clicked');
        });
        return button;
      },
      onRemove: () => undefined
    });

    L.control['directions'] = (opts) => {
      return new L.Control['Directions'](opts);
    };

    L.control['directions']({ position: 'bottomleft' }).addTo(this.map);
  }

  render() {
    return [
      <ion-card style={{'--background': 'white'}}>
        <div id="map" style={{'height': '50vh', 'display': 'block'}}/>
      </ion-card>
    ];
  }
}
