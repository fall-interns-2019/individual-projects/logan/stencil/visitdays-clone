import {Component, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'app-visit-tabs',
  styleUrl: 'app-visit-tabs.css'
})
export class AppVisitTabs {

  @Prop() collegeId: number;
  @State() selectedTab: 'daily' | 'events' | 'travel' = 'daily';

  tabComponents = {
    daily: () => <app-visit-daily-page collegeId={this.collegeId} />,
    events: () => <app-visit-events-page collegeId={this.collegeId} />,
    travel: () => <app-visit-travel-page collegeId={this.collegeId} />
  };

  async onIonSegmentChanged(event) {
    this.selectedTab = event.detail.value;
  }

  renderSelectedTab() {
    return this.tabComponents[this.selectedTab]();
  }

  render() {
    return [
      <app-itinerary-header/>,
      <ion-content color="light">
        <ion-segment style={{'background': 'white'}} value={this.selectedTab}
                     onIonChange={(evt) => this.onIonSegmentChanged(evt)}>
          <ion-segment-button value="daily">Daily Visits</ion-segment-button>
          <ion-segment-button value="events">Events</ion-segment-button>
          <ion-segment-button value="travel">Travel</ion-segment-button>
        </ion-segment>
        {this.renderSelectedTab()}
      </ion-content>
    ];
  }
}
