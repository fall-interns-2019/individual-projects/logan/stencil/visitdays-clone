import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'app-visit-events-page',
  styleUrl: 'app-visit-events-page.css'
})
export class AppVisitEventsPage {

  @Prop() collegeId: number;

  render() {
    return [
      <ion-item lines="none">
        <ion-icon name="calendar" slot="start"/>
        <ion-datetime class="ion-no-padding" display-format="MMM DD, YYYY"
                      min={new Date().toISOString()}
                      max={(() => {
                        const date = new Date();
                        date.setFullYear(date.getFullYear() + 1);
                        return date.toISOString();
                      })()} value={new Date().toISOString()}/>
      </ion-item>,
      <ion-card style={{'--background': 'white'}}>
        <ion-card-header>
          <ion-card-title class="ion-padding-bottom">
            Financial Aid Presentation
          </ion-card-title>
          <ion-chip outline>
            <ion-avatar>
              <img src="/assets/male-face-icon.png"/>
            </ion-avatar>
            <ion-label>
              John
            </ion-label>
          </ion-chip>
        </ion-card-header>
        <ion-card-content>
          <ion-button fill="outline">1:00pm - 2:00pm</ion-button>
        </ion-card-content>
      </ion-card>
    ];
  }
}
