import {Component, h} from '@stencil/core';
import {AppRoot} from "../app-root/app-root";

@Component({
  tag: 'app-itinerary-header',
  styleUrl: 'app-itinerary-header.css'
})
export class AppItineraryHeader {

  async onItineraryClicked() {
    await AppRoot.route(`/schedule`, true);
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-buttons slot="start">
            <ion-back-button/>
          </ion-buttons>
          <ion-title class="ion-text-center">
            <ion-label class="clickable" onClick={() => this.onItineraryClicked()}>
              My Itinerary
            </ion-label>
            <ion-chip color="light" onClick={() => this.onItineraryClicked()}>
              0
            </ion-chip>
          </ion-title>
        </ion-toolbar>
      </ion-header>
    ];
  }
}
