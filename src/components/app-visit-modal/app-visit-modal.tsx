import {Component, h, Prop} from '@stencil/core';
import {format} from 'date-fns';

@Component({
  tag: 'app-visit-modal',
  styleUrl: 'app-visit-modal.css'
})
export class AppVisitModal {

  @Prop() collegeId: number;
  @Prop() visitId: number;

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>
            Event Information
          </ion-title>
          <ion-buttons slot="primary">
            <ion-button fill="clear" color="primary">Cancel</ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content color="light">
        <ion-card style={{'--background': 'white'}}>
          <ion-card-content>
            <ion-item lines="none">
              <ion-icon name="clipboard" slot="start"/>
              Morning Campus Tour
            </ion-item>
            <ion-item lines="none">
              <ion-icon name="calendar" slot="start"/>
              {format(new Date(), 'eeee, MMMM d')}
            </ion-item>
            <ion-item lines="none">
              <ion-icon name="time" slot="start"/>
              9:00am - 10:00am
            </ion-item>
            <ion-item lines="none">
              <ion-icon name="pin" slot="start"/>
              Admissions Office
            </ion-item>
            <ion-item lines="none">
              <ion-icon name="people" slot="start"/>
              Sam and Erica
            </ion-item>
            <ion-label class="ion-padding" color="dark">
              Come visit our campus!
            </ion-label>
          </ion-card-content>
        </ion-card>
        <ion-card style={{'--background': 'white'}}>
          <ion-card-header>
            <ion-card-title>
              Personal Information
            </ion-card-title>
          </ion-card-header>
          <ion-card-content class="ion-no-padding">
            <form>
              <ion-list lines="none">
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">First name</ion-label>
                  <ion-input id="input-name" required type="text" color="dark"
                             placeholder="Sam"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Last name</ion-label>
                  <ion-input id="input-name" required type="text" color="dark"
                             placeholder="Birk"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Email</ion-label>
                  <ion-input id="input-name" required type="email" color="dark"
                             placeholder="sam@example.com"/>
                </ion-item>
                <ion-item lines="inset" class="ion-form-text ion-padding-horizontal">
                  <ion-label color="dark" position="stacked">Phone number</ion-label>
                  <ion-input id="input-name" required type="tel" color="dark"
                             placeholder="123-456-7890"/>
                </ion-item>
              </ion-list>
            </form>
            <br/>
            <ion-button id="button-register" class="ion-padding-vertical center-horiz half-width">
              Register
            </ion-button>
          </ion-card-content>
        </ion-card>
      </ion-content>
    ];
  }
}
