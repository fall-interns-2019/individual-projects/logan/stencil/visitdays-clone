import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'app-visit-daily-page',
  styleUrl: 'app-visit-daily-page.css'
})
export class AppVisitDailyPage {

  @Prop() collegeId: number;

  render() {
    return [
      <ion-item lines="none">
        <ion-icon name="calendar" slot="start"/>
        <ion-datetime class="ion-no-padding" display-format="MMM DD, YYYY"
                      min={new Date().toISOString()}
                      max={(() => {
                        const date = new Date();
                        date.setDate(date.getDate() + 14);
                        return date.toISOString();
                      })()} value={new Date().toISOString()}/>
      </ion-item>,
      <ion-card style={{'--background': 'white'}}>
        <img src="/assets/landscape.jpg"/>
        <ion-card-header>
          <ion-card-title class="ion-padding-bottom">
            Morning Campus Tour
          </ion-card-title>
          <ion-chip outline>
            <ion-avatar>
              <img src="/assets/male-face-icon.png"/>
            </ion-avatar>
            <ion-label>
              Sam
            </ion-label>
          </ion-chip>
          <ion-chip outline>
            <ion-avatar>
              <img src="/assets/female-face-icon.png"/>
            </ion-avatar>
            <ion-label>
              Erica
            </ion-label>
          </ion-chip>
        </ion-card-header>
        <ion-card-content>
          <ion-button fill="outline">9:00am - 10:00am</ion-button>
          <ion-button fill="outline">10:00am - 11:00am</ion-button>
          <ion-button fill="outline">11:00am - 12:00pm</ion-button>
        </ion-card-content>
      </ion-card>
    ];
  }
}
